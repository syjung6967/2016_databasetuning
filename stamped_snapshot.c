#include <stdlib.h>
#include <string.h>
#include "stamp_entity.h"

#define swap(a, b) \
  do { typeof(a) __tmp = (a); (a) = (b); (b) = __tmp; } while (0)

static void
stamped_snapshot_collect(struct stamped_snap_entity *arr,
                         struct stamped_snap_entity *copy,
                         int nr)
{
  int i;

  for (i = 0; i < nr; i++) {
    copy[i] = arr[i];
  }
}

/* buf size must be (2 * nr) and
 * moved size must be (nr + 1) to proof range out */
void
stamped_snapshot_scan(struct stamped_snap_entity *arr,
                      int nr,
                      struct stamped_snap_entity *buf,
                      char *moved,
                      struct stamped_snap *snap)
{
  struct stamped_snap_entity *old, *new;
  int flag;
  int i;

retry:
  old = buf;
  new = buf + nr;
  memset(moved, 0, sizeof(char) * (nr + 1));
  flag = 0;

  stamped_snapshot_collect(arr, old, nr);
  do {
    stamped_snapshot_collect(arr, new, nr);
    for (i = 0; i < nr; i++) {
      if (old[i].stamp != new[i].stamp) {
        if (moved[i]) {
          flag = i;
        }
        break;
      }
    }
    moved[i] = 1;
    swap(old, new);
  } while (!flag && i != nr);

  /* Currently, old is the newest, so swap old & new */
  swap(old, new);

  if (flag > 0) {
    /* copy the snap into memory of readers/writers */
    for (i = 0; i < nr; i++) {
      snap->snap[i] = new[flag].snap->snap[i];
    }
    /* If new[flag].snap is overwritten during this scan, retry */
    if (new[flag].stamp != new[flag].snap->stamp) {
      goto retry;
    }
  } else {
    for (i = 0; i < nr; i++) {
      snap->snap[i] = new[i].value;
    }
  }
}

void
stamped_snapshot_update(struct stamped_snap_entity *arr,
                        int nr,
                        void *value,
                        int me,
                        struct stamped_snap_entity *buf,
                        char *moved)
{
  struct stamped_snap_entity old, new;

  old = arr[me];

  new.stamp = old.stamp + 1;
  new.value = value;

  /* notify the change to other readers and writers */
  old.snap->next->stamp = new.stamp;
  new.snap = old.snap->next;

  stamped_snapshot_scan(arr, nr, buf, moved, new.snap);

  arr[me] = new;
}
