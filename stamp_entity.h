#ifndef _STAMP_ENTITY_H
#define _STAMP_ENTITY_H

struct stamped_entity {
  unsigned long long stamp;
  void *value;
};

struct stamped_snap {
  unsigned long long stamp;
  void **snap;
  struct stamped_snap *next;
};

struct stamped_snap_entity {
  unsigned long long stamp;
  void *value;
  struct stamped_snap *snap;    /* circular list to remember snaps */
};

#endif
