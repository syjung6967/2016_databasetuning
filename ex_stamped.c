#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "stamp_entity.h"
#include "entity.h"

#define READER_MAX 5000
#define WRITER_MAX 5000

/* remember SNAP_LIST_SIZE past snapshots per writer */
#define SNAP_LIST_SIZE 64

#define DUMMY_VALUE 0xABCDEF

/* volatile to prevent compiler optimization */
volatile enum state {
  STATE_WAIT_THREAD_CREATION,
  STATE_SLEEP,
  STATE_TERMINATE_THREAD
} state;

struct thread_data {
  int tid;
  int nr;
  struct stamped_snap_entity *value;
};

extern void **
stamped_snapshot_scan(struct stamped_snap_entity *arr,
                      int nr,
                      struct stamped_snap_entity *buf,
                      char *moved,
                      struct stamped_snap *snap);

extern void
stamped_snapshot_update(struct stamped_snap_entity *arr,
                        int nr,
                        void *value,
                        int me,
                        struct stamped_snap_entity *buf,
                        char *moved);

void *read_func(void *ptr)
{
  struct thread_data *thd;
  int nr;
  struct stamped_snap_entity *arr;
  struct stamped_snap_entity *buf;
  char *moved;
  struct stamped_snap *result;
  unsigned long long tot, *ret_tot;
  int i;

  /* wait for other readers and writers */
  while (state == STATE_WAIT_THREAD_CREATION);

  thd = (struct thread_data *) ptr;
  nr = thd->nr;
  arr = thd->value;
  buf = (struct stamped_snap_entity *)
        malloc(sizeof(struct stamped_snap_entity) * (2 * nr));
  moved = (char *) malloc(sizeof(char) * (nr + 1));
  result = (struct stamped_snap *) malloc(sizeof(struct stamped_snap));
  result->snap = (void **) malloc(sizeof(void *) * nr);
  tot = 0;

  while (state != STATE_TERMINATE_THREAD) {
    /* critical section start */
    stamped_snapshot_scan(arr, nr, buf, moved, result);
    /* critical section end */
    tot = tot + 1;
  }

  free(buf);
  free(moved);
  free(result->snap);
  free(result);

  ret_tot = (unsigned long long *) malloc(sizeof(unsigned long long));
  *ret_tot = tot;
  pthread_exit(ret_tot);
}

void *write_func(void *ptr)
{
  struct thread_data *thd;
  int nr;
  struct stamped_snap_entity *arr;
  int tid;
  struct stamped_snap_entity *buf;
  char *moved;
  unsigned long long tot, *ret_tot;

  /* wait for other readers and writers */
  while (state == STATE_WAIT_THREAD_CREATION);

  thd = (struct thread_data *) ptr;
  nr = thd->nr;
  arr = thd->value;
  tid = thd->tid;
  buf = (struct stamped_snap_entity *)
        malloc(sizeof(struct stamped_snap_entity) * (2 * nr));
  moved = (char *) malloc(sizeof(char) * (nr + 1));
  tot = 0;

  while (state != STATE_TERMINATE_THREAD) {
    /* critical section start */
    stamped_snapshot_update(arr, nr, (struct entity*) DUMMY_VALUE, tid,
                            buf, moved);
    /* critical section end */
    tot = tot + 1;
  }

  free(buf);
  free(moved);

  ret_tot = (unsigned long long *) malloc(sizeof(unsigned long long));
  *ret_tot = tot;
  pthread_exit(ret_tot);
}

static void validate_args(unsigned int nr_reader,
                          unsigned int nr_writer)
{
  if (nr_reader > READER_MAX) {
    fprintf(stderr, "Too many readers!\n");
    exit(EXIT_FAILURE);
  }

  if (nr_writer > WRITER_MAX) {
    fprintf(stderr, "Too many writers!\n");
    exit(EXIT_FAILURE);
  }
}

static void
init_arr(struct stamped_snap_entity *arr, unsigned int nr_writer)
{
  int i, j;
  struct stamped_snap *curr, *next;

  for (i = 0; i < nr_writer; i++) {
    arr[i].stamp = 0;
    arr[i].snap = (struct stamped_snap *) malloc(sizeof(struct stamped_snap));
    arr[i].snap->stamp = 0;
    arr[i].snap->snap = (void **) malloc(sizeof(void *) * nr_writer);
    curr = arr[i].snap;
    for (j = 1; j < SNAP_LIST_SIZE; j++) {
      next = (struct stamped_snap *) malloc(sizeof(struct stamped_snap));
      next->stamp = 0;
      next->snap = (void **) malloc(sizeof(void *) * nr_writer);
      curr->next = next;
      curr = next;
    }
    curr->next = arr[i].snap;
  }
}

static void
free_arr(struct stamped_snap_entity *arr, unsigned int nr_writer)
{
  int i, j;
  struct stamped_snap *curr, *next;

  for (i = 0; i < nr_writer; i++) {
    curr = arr[i].snap;
    for (j = 0; j < SNAP_LIST_SIZE; j++) {
      next = curr->next;
      free(curr->snap);
      free(curr);
      curr = next;
    }
  }

  free(arr);
}

static void
print_results(unsigned int nr_reader, unsigned int nr_writer,
              pthread_t *readers, pthread_t *writers)
{
  unsigned long long reader_tot, writer_tot;
  unsigned long long *tot;
  int i;

  printf("%d Reader Thread(s)\n", nr_reader);
  reader_tot = 0;
  for (i = 0; i < nr_reader; i++) {
    pthread_join(readers[i], (void **) &tot);
    printf("Reader #%04d: %llu\n", i, *tot);
    reader_tot = reader_tot + *tot;
    free(tot);
  }
  printf ("\n");
  printf("%d Writer Thread(s)\n", nr_writer);
  writer_tot = 0;
  for (i = 0; i < nr_writer; i++) {
    pthread_join(writers[i], (void **) &tot);
    printf("Writer #%04d: %llu\n", i, *tot);
    writer_tot = writer_tot + *tot;
    free(tot);
  }
  printf ("\n");
  printf("Reader Total: %llu\n", reader_tot);
  printf("Writer Total: %llu\n", writer_tot);
}

int main(int argc, char *argv[])
{
  pthread_t *readers, *writers;
  unsigned int nr_reader, nr_writer;
  struct thread_data *readers_thd, *writers_thd;
  struct stamped_snap_entity *arr;
  int i;

  if (argc != 3) {
    printf("Usage: %s <reader(s)> <writer(s)>\n", argv[0]);
  } else {
    sscanf(argv[1], "%u", &nr_reader);
    sscanf(argv[2], "%u", &nr_writer);
    validate_args(nr_reader, nr_writer);

    arr = (struct stamped_snap_entity *)
          malloc(sizeof(struct stamped_snap_entity) * nr_writer);
    init_arr(arr, nr_writer);

    readers_thd = (struct thread_data *)
                  malloc(sizeof(struct thread_data) * nr_reader);
    writers_thd = (struct thread_data *)
                  malloc(sizeof(struct thread_data) * nr_writer);

    state = STATE_WAIT_THREAD_CREATION;

    readers = (pthread_t *) malloc(sizeof(pthread_t) * nr_reader);
    writers = (pthread_t *) malloc(sizeof(pthread_t) * nr_writer);
    for (i = 0; i < nr_reader; i++) {
      readers_thd[i].nr = nr_writer;
      readers_thd[i].value = arr;
      readers_thd[i].tid = i;
      pthread_create(&readers[i], NULL, read_func, (void *) &readers_thd[i]);
    }
    for (i = 0; i < nr_writer; i++) {
      writers_thd[i].nr = nr_writer;
      writers_thd[i].value = arr;
      writers_thd[i].tid = i;
      pthread_create(&writers[i], NULL, write_func, (void *) &writers_thd[i]);
    }

    /* main thread wakes up after 10 seconds */
    printf("Wait 10 seconds...\n");
    state = STATE_SLEEP;
    sleep(10);
    state = STATE_TERMINATE_THREAD;

    /* join and print the results */
    print_results(nr_reader, nr_writer, readers, writers);

    free(readers_thd);
    free(writers_thd);
    free_arr(arr, nr_writer);
    free(readers);
    free(writers);
  }

  return 0;
}
