#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "entity.h"

#define READER_MAX 5000
#define WRITER_MAX 5000

#define DUMMY_VALUE 0xABCDEF

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

/* volatile to prevent compiler optimization */
volatile enum state {
  STATE_WAIT_THREAD_CREATION,
  STATE_SLEEP,
  STATE_TERMINATE_THREAD
} state;

struct thread_data {
  int tid;
  int nr;
  struct entity *value;
};

void *read_func(void *ptr)
{
  struct thread_data *thd;
  int nr;
  struct entity *arr;
  struct entity *result;
  unsigned long long tot, *ret_tot;
  int i;

  /* wait for other readers and writers */
  while (state == STATE_WAIT_THREAD_CREATION);

  thd = (struct thread_data *) ptr;
  nr = thd->nr;
  arr = thd->value;
  result = (struct entity *) malloc(sizeof(struct entity) * nr);
  tot = 0;

  while (state != STATE_TERMINATE_THREAD) {
    pthread_mutex_lock(&mutex);
    /* critical section start */
    for (i = 0; i < nr; i++) {
      result[i] = arr[i];
    }
    /* critical section end */
    pthread_mutex_unlock(&mutex);
    tot = tot + 1;
  }

  free(result);

  ret_tot = (unsigned long long *) malloc(sizeof(unsigned long long));
  *ret_tot = tot;
  pthread_exit(ret_tot);
}

void *write_func(void *ptr)
{
  struct thread_data *thd;
  int nr;
  struct entity *arr;
  int tid;
  unsigned long long tot, *ret_tot;

  /* wait for other readers and writers */
  while (state == STATE_WAIT_THREAD_CREATION);

  thd = (struct thread_data *) ptr;
  nr = thd->nr;
  arr = thd->value;
  tid = thd->tid;
  tot = 0;

  while (state != STATE_TERMINATE_THREAD) {
    pthread_mutex_lock(&mutex);
    /* critical section start */
    arr[tid].dummy_value = DUMMY_VALUE;
    /* critical section end */
    pthread_mutex_unlock(&mutex);
    tot = tot + 1;
  }

  ret_tot = (unsigned long long *) malloc(sizeof(unsigned long long));
  *ret_tot = tot;
  pthread_exit(ret_tot);
}

static void validate_args(unsigned int nr_reader,
                          unsigned int nr_writer)
{
  if (nr_reader > READER_MAX) {
    fprintf(stderr, "Too many readers!\n");
    exit(EXIT_FAILURE);
  }

  if (nr_writer > WRITER_MAX) {
    fprintf(stderr, "Too many writers!\n");
    exit(EXIT_FAILURE);
  }
}

static void
print_results(unsigned int nr_reader, unsigned int nr_writer,
              pthread_t *readers, pthread_t *writers)
{
  unsigned long long reader_tot, writer_tot;
  unsigned long long *tot;
  int i;

  printf("%d Reader Thread(s)\n", nr_reader);
  reader_tot = 0;
  for (i = 0; i < nr_reader; i++) {
    pthread_join(readers[i], (void **) &tot);
    printf("Reader #%04d: %llu\n", i, *tot);
    reader_tot = reader_tot + *tot;
    free(tot);
  }
  printf ("\n");
  printf("%d Writer Thread(s)\n", nr_writer);
  writer_tot = 0;
  for (i = 0; i < nr_writer; i++) {
    pthread_join(writers[i], (void **) &tot);
    printf("Writer #%04d: %llu\n", i, *tot);
    writer_tot = writer_tot + *tot;
    free(tot);
  }
  printf ("\n");
  printf("Reader Total: %llu\n", reader_tot);
  printf("Writer Total: %llu\n", writer_tot);
}

int main(int argc, char *argv[])
{
  pthread_t *readers, *writers;
  unsigned int nr_reader, nr_writer;
  struct thread_data *readers_thd, *writers_thd;
  struct entity *arr;
  int i;

  if (argc != 3) {
    printf("Usage: %s <reader(s)> <writer(s)>\n", argv[0]);
  } else {
    sscanf(argv[1], "%u", &nr_reader);
    sscanf(argv[2], "%u", &nr_writer);
    validate_args(nr_reader, nr_writer);

    arr = (struct entity *) malloc(sizeof(struct entity) * nr_writer);
    readers_thd = (struct thread_data *)
                  malloc(sizeof(struct thread_data) * nr_reader);
    writers_thd = (struct thread_data *)
                  malloc(sizeof(struct thread_data) * nr_writer);

    state = STATE_WAIT_THREAD_CREATION;

    readers = (pthread_t *) malloc(sizeof(pthread_t) * nr_reader);
    writers = (pthread_t *) malloc(sizeof(pthread_t) * nr_writer);
    for (i = 0; i < nr_reader; i++) {
      readers_thd[i].nr = nr_writer;
      readers_thd[i].value = arr;
      readers_thd[i].tid = i;
      pthread_create(&readers[i], NULL, read_func, (void *) &readers_thd[i]);
    }
    for (i = 0; i < nr_writer; i++) {
      writers_thd[i].nr = nr_writer;
      writers_thd[i].value = arr;
      writers_thd[i].tid = i;
      pthread_create(&writers[i], NULL, write_func, (void *) &writers_thd[i]);
    }

    /* main thread wakes up after 10 seconds */
    printf("Wait 10 seconds...\n");
    state = STATE_SLEEP;
    sleep(10);
    state = STATE_TERMINATE_THREAD;

    /* join and print the result */
    print_results(nr_reader, nr_writer, readers, writers);

    free(readers_thd);
    free(writers_thd);
    free(arr);
    free(readers);
    free(writers);
  }

  return 0;
}
