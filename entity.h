#ifndef _ENTITY_H
#define _ENTITY_H

struct entity {
  int dummy_value;
};

static inline int
entity_equals(const struct entity *s,
              const struct entity *t)
{
  return s->dummy_value == t->dummy_value;
}

#endif
