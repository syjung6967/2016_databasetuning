====================
       README
====================

--------
Overview
--------
This is the README for the first homework to measure performance between
two applications: one utilizes mutex defined by POSIX, the other one utilizes
wait-free algorithm described in textbook Section 4.3.2. The performance
metric is how many snapshots are performed by readers according to the number
of readers and writers. Test the applications by increasing the number of
writers with one reader.

------
Design
------
The structure of StampedSnap was changed from
------------------
|     |     |    |
|stamp|value|snap|
|     |     |    |
------------------
into
----------------------------------------------------
|     |     |--------------------------------------|
|stamp|value||stamp,snap|stamp,snap|...|stamp,snap||
|     |     |--------------------------------------|
----------------------------------------------------
to reduce memory allocation.

We do not know how long the snap is preserved because it is depends on
reader, so we cannot free the memory at specified conditions. Therefore,
a lot of memory are wasted. To prevent this problem, the snaps are copied
into pre-allocated memory, though scan() finds a snap of a moved thread,
and the memory are reused until the program terminates. Unless a writer
thread updates its value more than SNAP_LIST_SIZE times during the copy
process, the copied snap is valid.

Writer changes the stamp of next snap into the stamp of new stamp first
and then scan(), and writer updates snap at last. When a moved thread
updated a snap that was copying from other threads, the others can
notice the change by comparing a stamp of the collected snap to a stamp
of snap of the collected snap.

If there are the updates more than SNAP_LIST_SIZE, scan() must be retried.

------------
Requirements
------------
gcc: to build programs
gle-graphics: to draw graphs

-----
Build
-----
Use the following command to build the programs.
$ ./build

Use the following command to clean them.
$ ./build clean

---
Run
---
Use the following command to test 20 times for each predefined case,
and then gather information, and consequently print box plots.

$ cd ./figures && ./run

Simple usage
$ ./{mutex,stamped} <reader(s)> <writer(s)>
